import http from "k6/http";

export const options = {
    vus : 30,
    duration : '10s',
    tresholds: {
        http_req_dauration:[{
            tresholds : 'p(90) < 10',
            abortOnFail : true,
        }],
    }
};

export default function(){
    http.get('https://test-api.k6.io/public/crocodiles/1/');
}