import http from "k6/http";
import { check } from "k6";
import { TableBodyElement } from "k6/html";


export default function(){
    const res = http.get('http://test.k6.io/');
    check (res, {
        'status website is 200' : (r) => r.status === 200,
        'status website is 201' : (r) => r.status ===201,
        'check body size ' : (r) => r.body.length == 11105,
    });
}
