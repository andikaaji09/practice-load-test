import { sleep } from "k6";
import http from "k6/http"


export const options = {
    vus      : 10,
    duration : '10s',
    tresholds: {
       checks:['rate>0.9'],
    },
};

export default function(){
    const res = http.get('http://httpbin.test.k6.io');

    check(res, {
        'status is 500': (r) => r.status == 500,
    });
    sleep(1);
};