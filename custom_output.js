import http from "k6/http";
//input trend 
import { Counter, Trend } from "k6/metrics";

const mytrend = new Trend('Waiting_time');
const mycounter = new Counter('counter_time');

// export default function(){
//     const res = http.get('http://httpbin.test.k6.io');
//     console.log('the spend time is' + String(res.timings.duration)+ 'ms');//untuk custom total duration yang diinginkan
//     //pakai res.timings.duration
// };

export default function(){
    const r = http.get('https://httpbin.test.k6.io');
    mytrend.add(r.timings.waiting);
    console.log(mytrend.name)
    //
    mycounter.add(1);
    mycounter.add(2);
}
