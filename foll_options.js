import http from "k6/http";
import { sleep } from "k6";


export const options = {
    hosts : {'test.k6.io':'1.2.3.4'},
    stages: [
        {duration : '1m', target: 10},
        {duration : '1m', target: 20},
        {duration : '1m', target: 0},
    ],
    tresholds : {
        http_req_duration : [
            'avg<100',
            'p(90)<200',
            'p(95)>500',
        ],
        noConnectionReUse : true,
        userAgent : 'MyK6UserAgentString/1.0',
    },
    dns : {
        ttl : '1m',
        select : 'roundRobin',
        policy : 'any'
    },
};

export default function(){
    http.get('http://test.k6.io/');
};
