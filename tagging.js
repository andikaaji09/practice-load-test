import http from "k6/http";
import { check } from "k6";
import { Trend } from "k6/metrics";



const mytrend = new Trend ('my_trend');

export default function() {
    const res = http.get('http://httpbin.test.k6.io/',{
        tags : {
            my_tag : "i'm tag",
        }
    });

    check(res, {'status is 200': (r) => r.status===200},
    {my_tag : "i'm tag"});
    
    //tag custom metrics 
    mytrend.add(res.timings.connecting, {
    my_tag: "I'm a tag"     
    })
};
