import {check, sleep} from 'k6';
import http from 'k6/http';

export const options = {
    stages: [
        {duration: '5s', target : 20},
        {duration: '5s', target : 10},
        {duration: '5s', target : 10},
    ],
    vus: 20
};

export default function () {
    const res = http.get('https://httpbin.test.k6.io/');
    check(res, { 'status was 200': (r) => r.status == 200 });
    sleep(1);
}

