import http from "k6/http";
import { check, group, sleep } from "k6";
import exec from "k6/execution";


export const options = {
    tresholds: {
        'http_reqs{container_group:main}' :['count==3'],
        'http_req_duration{container_group:main}' :['max<1000'],
    },

}

export default function(){
    exec.vu.tags.containerGroup = 'main';

    group('main', function(){
        http.get('https://test.k6.io');
    });
        http.get('https://test-api.k6.io');
    
    delete exec.vu.tags.containerGroup;
    http.get('https://httpbin.test.k6.io/delay/3');
};